<?php

namespace Mediadiv\UploadBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MediadivUploadBundle:Default:index.html.twig', array('name' => $name));
    }
}
