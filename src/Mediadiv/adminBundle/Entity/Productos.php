<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productos
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Productos {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

 
    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="unidadPorPack", type="string", length=255, nullable=true)
     */
    private $unidadPorPack;

    /**
     * @var string
     *
     * @ORM\Column(name="material", type="string", length=255, nullable=true)
     */
    private $material;

    /**
     * @var string
     *
     * @ORM\Column(name="talla", type="string", length=255, nullable=true)
     */
    private $talla;


    

    /**
     * @var string
     *
     * @ORM\Column(name="cuidado", type="text", nullable=true)
     */
    private $cuidado;

    /**
     * @var integer
     *
     * @ORM\Column(name="stock", type="integer", nullable=true)
     */
    private $stock;

    /**
     * @var string
     *
     * @ORM\Column(name="estado", type="string", length=255, nullable=true)
     */
    private $estado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="date")
     */
    private $fechaIngreso;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoProducto", mappedBy="productos" , cascade={"remove"})
     */
    private $fotoproducto;
    
     /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoSliderProducto", mappedBy="productos" , cascade={"remove"})
     */
    private $fotosliderproducto;


    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\SubCategorias", inversedBy="productos" )
     * @ORM\JoinColumn(name="subcategorias_id", referencedColumnName="id")
     */



    private $subcategorias;




      /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Calce", inversedBy="productos" )
     * @ORM\JoinColumn(name="calce_id", referencedColumnName="id")
     */


    private $calce;

    


     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\SubCalce", inversedBy="productos" )
     * @ORM\JoinColumn(name="subcalce_id", referencedColumnName="id")
     */



    private $subcalce;





       /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Linea", inversedBy="productos" )
     * @ORM\JoinColumn(name="linea_id", referencedColumnName="id")
     */


    private $linea;

    

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fotoproducto = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fotosliderproducto = new \Doctrine\Common\Collections\ArrayCollection();
    }

   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Productos
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Productos
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set unidadPorPack
     *
     * @param string $unidadPorPack
     * @return Productos
     */
    public function setUnidadPorPack($unidadPorPack)
    {
        $this->unidadPorPack = $unidadPorPack;

        return $this;
    }

    /**
     * Get unidadPorPack
     *
     * @return string 
     */
    public function getUnidadPorPack()
    {
        return $this->unidadPorPack;
    }

    /**
     * Set material
     *
     * @param string $material
     * @return Productos
     */
    public function setMaterial($material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return string 
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set talla
     *
     * @param string $talla
     * @return Productos
     */
    public function setTalla($talla)
    {
        $this->talla = $talla;

        return $this;
    }

    /**
     * Get talla
     *
     * @return string 
     */
    public function getTalla()
    {
        return $this->talla;
    }

    /**
     * Set cuidado
     *
     * @param string $cuidado
     * @return Productos
     */
    public function setCuidado($cuidado)
    {
        $this->cuidado = $cuidado;

        return $this;
    }

    /**
     * Get cuidado
     *
     * @return string 
     */
    public function getCuidado()
    {
        return $this->cuidado;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     * @return Productos
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer 
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Productos
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Productos
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Add fotoproducto
     *
     * @param \Mediadiv\adminBundle\Entity\FotoProducto $fotoproducto
     * @return Productos
     */
    public function addFotoproducto(\Mediadiv\adminBundle\Entity\FotoProducto $fotoproducto)
    {
        $this->fotoproducto[] = $fotoproducto;

        return $this;
    }

    /**
     * Remove fotoproducto
     *
     * @param \Mediadiv\adminBundle\Entity\FotoProducto $fotoproducto
     */
    public function removeFotoproducto(\Mediadiv\adminBundle\Entity\FotoProducto $fotoproducto)
    {
        $this->fotoproducto->removeElement($fotoproducto);
    }

    /**
     * Get fotoproducto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotoproducto()
    {
        return $this->fotoproducto;
    }

    /**
     * Add fotosliderproducto
     *
     * @param \Mediadiv\adminBundle\Entity\FotoSliderProducto $fotosliderproducto
     * @return Productos
     */
    public function addFotosliderproducto(\Mediadiv\adminBundle\Entity\FotoSliderProducto $fotosliderproducto)
    {
        $this->fotosliderproducto[] = $fotosliderproducto;

        return $this;
    }

    /**
     * Remove fotosliderproducto
     *
     * @param \Mediadiv\adminBundle\Entity\FotoSliderProducto $fotosliderproducto
     */
    public function removeFotosliderproducto(\Mediadiv\adminBundle\Entity\FotoSliderProducto $fotosliderproducto)
    {
        $this->fotosliderproducto->removeElement($fotosliderproducto);
    }

    /**
     * Get fotosliderproducto
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotosliderproducto()
    {
        return $this->fotosliderproducto;
    }

    /**
     * Set subcategorias
     *
     * @param \Mediadiv\adminBundle\Entity\SubCategorias $subcategorias
     * @return Productos
     */
    public function setSubcategorias(\Mediadiv\adminBundle\Entity\SubCategorias $subcategorias = null)
    {
        $this->subcategorias = $subcategorias;

        return $this;
    }

    /**
     * Get subcategorias
     *
     * @return \Mediadiv\adminBundle\Entity\SubCategorias 
     */
    public function getSubcategorias()
    {
        return $this->subcategorias;
    }

    /**
     * Set calce
     *
     * @param \Mediadiv\adminBundle\Entity\Calce $calce
     * @return Productos
     */
    public function setCalce(\Mediadiv\adminBundle\Entity\Calce $calce = null)
    {
        $this->calce = $calce;

        return $this;
    }

    /**
     * Get calce
     *
     * @return \Mediadiv\adminBundle\Entity\Calce 
     */
    public function getCalce()
    {
        return $this->calce;
    }

    /**
     * Set subcalce
     *
     * @param \Mediadiv\adminBundle\Entity\SubCalce $subcalce
     * @return Productos
     */
    public function setSubcalce(\Mediadiv\adminBundle\Entity\SubCalce $subcalce = null)
    {
        $this->subcalce = $subcalce;

        return $this;
    }

    /**
     * Get subcalce
     *
     * @return \Mediadiv\adminBundle\Entity\SubCalce 
     */
    public function getSubcalce()
    {
        return $this->subcalce;
    }

    /**
     * Set linea
     *
     * @param \Mediadiv\adminBundle\Entity\Linea $linea
     * @return Productos
     */
    public function setLinea(\Mediadiv\adminBundle\Entity\Linea $linea = null)
    {
        $this->linea = $linea;

        return $this;
    }

    /**
     * Get linea
     *
     * @return \Mediadiv\adminBundle\Entity\Linea 
     */
    public function getLinea()
    {
        return $this->linea;
    }
}
