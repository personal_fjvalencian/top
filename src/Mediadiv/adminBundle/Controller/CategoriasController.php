<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session;
use Mediadiv\adminBundle\Entity\Categorias;
use Mediadiv\adminBundle\Form\CategoriasType;

/**
 * Categorias controller.
 *
 */
class CategoriasController extends Controller {

    /**
     * Lists all Categorias entities.
     *
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entities = $em->getRepository('MediadivadminBundle:Categorias')->findAll();

            return $this->render('MediadivadminBundle:Categorias:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Categorias entity.
     *
     */
    public function createAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = new Categorias();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entity->setFecha(new \DateTime());

                $em->persist($entity);
                $em->flush();


                $this->get('session')->getFlashBag()->add(
                        'categoria', 'Tu Categoria ah sido Guardada'
                );

                return $this->redirect($this->generateUrl('categorias'));
            }

            return $this->render('MediadivadminBundle:Categorias:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {

            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Categorias entity.
     *
     * @param Categorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Categorias $entity) {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('categorias_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array(
                'class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Categorias entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = new Categorias();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Categorias:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Categorias entity.
     *
     */
    public function editAction($id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entity = $em->getRepository('MediadivadminBundle:Categorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Categorias entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Categorias:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Categorias entity.
     *
     * @param Categorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Categorias $entity) {
        $form = $this->createForm(new CategoriasType(), $entity, array(
            'action' => $this->generateUrl('categorias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Editar',
            'attr' => array(
                'class' => 'btn btn-success'
            )
        ));

        return $form;
    }

    /**
     * Edits an existing Categorias entity.
     *
     */
    public function updateAction(Request $request, $id) {


        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Categorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Categorias entity.');
            }


            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'categoria', 'Tu Categoria ah sido Editada'
                );

                return $this->redirect($this->generateUrl('categorias'));
            }

            return $this->render('MediadivadminBundle:Categorias:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function eliminarCategoriaAction($id) {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'categoria', 'Tu Categoria ah sido Eliminada'
        );

        return $this->redirect($this->generateUrl('categorias'));
    }

}
