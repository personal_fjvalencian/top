<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubCalce
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\adminBundle\Entity\SubCalceRepository")
 */
class SubCalce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


   


   /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    

     /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Calce", inversedBy="subcalce" )
     * @ORM\JoinColumn(name="calce_id", referencedColumnName="id")
     */

    private $calce;


    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\Productos", mappedBy="subcalce" , cascade={"remove"})
     */

    private $productos;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }




    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return SubCalce
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set calce
     *
     * @param \Mediadiv\adminBundle\Entity\Calce $calce
     * @return SubCalce
     */
    public function setCalce(\Mediadiv\adminBundle\Entity\Calce $calce = null)
    {
        $this->calce = $calce;

        return $this;
    }

    /**
     * Get calce
     *
     * @return \Mediadiv\adminBundle\Entity\Calce 
     */
    public function getCalce()
    {
        return $this->calce;
    }

    /**
     * Add productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     * @return SubCalce
     */
    public function addProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     */
    public function removeProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
