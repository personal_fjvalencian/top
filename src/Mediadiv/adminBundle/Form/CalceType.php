<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CalceType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array(
                    'label' => 'Nombre: ',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))


            ->add('novedad', 'choice', array(
                'label' => 'Seleccione novedad: ',
                'choices' => array('' => 'opcion' , 'novedad' => 'novedad' , 'no novedad' => 'no novedad'),
                'attr' => array(
                    'class' => 'form-control'
                    )))

            

            ->add('descripcion', 'textarea', array(
                'label' => 'Descripción: ',
                'attr' => array(
                'class' => 'form-control jqte-test'
                )
                ))
                 ;

        
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\Calce'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_calce';
    }
}
