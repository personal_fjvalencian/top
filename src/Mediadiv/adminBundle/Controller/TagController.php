<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\adminBundle\Entity\Tag;
use Mediadiv\adminBundle\Form\TagType;

/**
 * Tag controller.
 *
 */
class TagController extends Controller {

    /**
     * Lists all Tag entities.
     *
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entities = $em->getRepository('MediadivadminBundle:Tag')->findAll();

            return $this->render('MediadivadminBundle:Tag:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Tag entity.
     *
     */
    public function createAction(Request $request) {
        $entity = new Tag();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setFecha(new \DateTime());
            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'tag', 'Tu Tag ah sido Guardada'
            );

            return $this->redirect($this->generateUrl('admin_tag'));
        }

        return $this->render('MediadivadminBundle:Tag:new.html.twig', array(
                    'entity' => $entity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tag $entity) {
        $form = $this->createForm(new TagType(), $entity, array(
            'action' => $this->generateUrl('admin_tag_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Guardar',
            'attr' => array('class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Tag entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entity = new Tag();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Tag:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Tag entity.
     *
     */
    public function editAction($id) {


        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Tag')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tag entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Tag:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {

            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Tag entity.
     *
     * @param Tag $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Tag $entity) {
        $form = $this->createForm(new TagType(), $entity, array(
            'action' => $this->generateUrl('admin_tag_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar',
            'attr' => array('class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Edits an existing Tag entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivadminBundle:Tag')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Tag entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'tag', 'Tu Tag ah sido Editada'
            );
            return $this->redirect($this->generateUrl('admin_tag'));
        }

        return $this->render('MediadivadminBundle:Tag:edit.html.twig', array(
                    'entity' => $entity,
                    'edit_form' => $editForm->createView(),
        ));
    }

    #Accion eliminar categoria.

    public function eliminarTagAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Tag')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'tag', 'Tu Tag ah sido Eliminada'
        );

        return $this->redirect($this->generateUrl('admin_tag'));
    }

}
