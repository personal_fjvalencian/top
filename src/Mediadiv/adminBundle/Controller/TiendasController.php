<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;
use Mediadiv\adminBundle\Entity\Tiendas;
use Mediadiv\adminBundle\Form\TiendasType;

/**
 * Tiendas controller.
 *
 */
class TiendasController extends Controller {

    /**
     * Lists all Tiendas entities.
     *
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $em = $this->getDoctrine()->getManager();

            $entities = $em->getRepository('MediadivadminBundle:Tiendas')->findAll();

            return $this->render('MediadivadminBundle:Tiendas:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Tiendas entity.
     *
     */
    public function createAction(Request $request) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = new Tiendas();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entity->setFechaIngreso(new \DateTime());
                $em->persist($entity);
                $em->flush();


                $idTienda = $entity->getId();

                $session->set('idTienda', $idTienda);

                $this->get('session')->getFlashBag()->add(
                        'tienda', 'Tu Tienda ah sido Guardada'
                );
                return $this->redirect($this->generateUrl('tiendas_vistaUploadTiendas'));
            }

            return $this->render('MediadivadminBundle:Tiendas:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Tiendas entity.
     *
     * @param Tiendas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Tiendas $entity) {
        $form = $this->createForm(new TiendasType(), $entity, array(
            'action' => $this->generateUrl('tiendas_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Subir foto', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Tiendas entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = new Tiendas();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Tiendas:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Tiendas entity.
     *
     */
    public function editAction($id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Tiendas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tiendas entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Tiendas:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Tiendas entity.
     *
     * @param Tiendas $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Tiendas $entity) {
        $form = $this->createForm(new TiendasType(), $entity, array(
            'action' => $this->generateUrl('tiendas_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-default')));

        return $form;
    }

    /**
     * Edits an existing Tiendas entity.
     *
     */
    public function updateAction(Request $request, $id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {


            $entity = $em->getRepository('MediadivadminBundle:Tiendas')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Tiendas entity.');
            }

            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $idTienda = $entity->getId();

                $session->set('idTienda', $idTienda);


                $this->get('session')->getFlashBag()->add(
                        'tienda', 'Tu Tienda ah sido Editada'
                );

                return $this->redirect($this->generateUrl('tiendas_vistaUploadTiendas'));
            }

            return $this->render('MediadivadminBundle:Tiendas:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function eliminarTiendaAction($id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Tiendas')->findOneby(array('id' => $id));

            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'tienda', 'Tu Tienda ah sido Eliminada'
            );

            return $this->redirect($this->generateUrl('tiendas'));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function vistaUploadTiendasAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $idTienda = $session->get('idTienda');

            return $this->render('MediadivadminBundle:Tiendas:vistaUploadTienda.html.twig', array('idTienda' => $idTienda));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function vistaUploadTiendaDosAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $idTienda = $session->get('idTienda');

            return $this->render('MediadivadminBundle:Tiendas:vistaUploadTiendaDos.html.twig', array('idTienda' => $idTienda));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function uploadTiendaAction() {

        $session = $this->getRequest()->getSession();


        $id = $session->get('idTienda');

        $idTienda = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'campanias/' . $idTienda . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'tiendas/' . $idTienda . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();

            $tienda = $em->getRepository('MediadivadminBundle:Tiendas')->findOneBy(array('id' => $idTienda));

            $tienda->setUrl1($fileName);
            $em->persist($tienda);
            $em->flush();

            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }


    public function uploadTiendaDosAction() {

        $session = $this->getRequest()->getSession();


        $id = $session->get('idTienda');

        $idTienda = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'campanias/' . $idTienda . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'tiendas/' . $idTienda . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();

            $tienda = $em->getRepository('MediadivadminBundle:Tiendas')->findOneBy(array('id' => $idTienda));

            $tienda->setUrl2($fileName);
            $em->persist($tienda);
            $em->flush();

            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }






 public function uploadTiendaTumbAction() {

        $session = $this->getRequest()->getSession();


        $id = $session->get('idTienda');

        $idTienda = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'tiendas/tumb/' . $idTienda . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'tiendas/tumb/' . $idTienda . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();

            echo 'id Tienda ' . $idTienda;

            

            $tienda = $em->getRepository('MediadivadminBundle:Tiendas')->findOneBy(array('id' => $idTienda));

            $tienda->setTumb($fileName);
            $em->persist($tienda);
            $em->flush();


            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

}
