<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubCategoriasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', 'text', array('attr' => array('class' => 'form-control')))
            ->add('categorias',
                  'entity',
                   array(
                       'class' => 'MediadivadminBundle:Categorias',
                       'property' => 'nombre',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Categoria',
                       'required' => false,
                       'empty_value' => 'Selecciona la categoria',
                       'empty_data' => null,
                   )
                )
            ->add('novedad', 'choice', array(
                'label' => 'Seleccione novedad: ',
                'choices' => array('' => 'opcion' , 'novedad' => 'novedad' , 'no novedad' => 'no novedad'),
                'attr' => array(
                    'class' => 'form-control'
                    )))

            ->add('tag',
                  'entity',
                   array(
                       'class' => 'MediadivadminBundle:Tag',
                       'property' => 'descripcion',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Tag',
                       'required' => false,
                       'empty_value' => 'Selecciona el tag',
                       'empty_data' => null,
                   )
                );
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\SubCategorias'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_subcategorias';
    }
}
