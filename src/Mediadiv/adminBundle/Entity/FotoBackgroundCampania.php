<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotoBackgroundCampania
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FotoBackgroundCampania
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="datetime")
     */
    private $fechaIngreso;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Campania", inversedBy="fotobackgroundcampania")
     * @ORM\JoinColumn(name="campania_id", referencedColumnName="id")
     *
     */
    private $campania;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FotoBackgroundCampania
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return FotoBackgroundCampania
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set campania
     *
     * @param \Mediadiv\adminBundle\Entity\Campania $campania
     * @return FotoBackgroundCampania
     */
    public function setCampania(\Mediadiv\adminBundle\Entity\Campania $campania = null)
    {
        $this->campania = $campania;

        return $this;
    }

    /**
     * Get campania
     *
     * @return \Mediadiv\adminBundle\Entity\Campania 
     */
    public function getCampania()
    {
        return $this->campania;
    }
}
