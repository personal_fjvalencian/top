<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campania
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Campania {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255 , nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text" , nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255 , nullable=true )
     */
    private $titulo;



      /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=255 , nullable=true )
     */
    private $tipo;



    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=455 , nullable=true)
     */



    private $url;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="date")
     */
    private $fechaIngreso;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoCampania", mappedBy="campania" , cascade={"remove"})
     */
    private $fotocampania;
    
    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoBackgroundCampania", mappedBy="campania" , cascade={"remove"})
     */
    private $fotobackgroundcampania;
    
    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoCampaniaHome", mappedBy="campania" , cascade={"remove"})
     */
    private $fotocampaniahome;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fotocampania = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fotobackgroundcampania = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fotocampaniahome = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Campania
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Campania
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Campania
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Campania
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Campania
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Campania
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Add fotocampania
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCampania $fotocampania
     * @return Campania
     */
    public function addFotocampanium(\Mediadiv\adminBundle\Entity\FotoCampania $fotocampania)
    {
        $this->fotocampania[] = $fotocampania;

        return $this;
    }

    /**
     * Remove fotocampania
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCampania $fotocampania
     */
    public function removeFotocampanium(\Mediadiv\adminBundle\Entity\FotoCampania $fotocampania)
    {
        $this->fotocampania->removeElement($fotocampania);
    }

    /**
     * Get fotocampania
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotocampania()
    {
        return $this->fotocampania;
    }

    /**
     * Add fotobackgroundcampania
     *
     * @param \Mediadiv\adminBundle\Entity\FotoBackgroundCampania $fotobackgroundcampania
     * @return Campania
     */
    public function addFotobackgroundcampanium(\Mediadiv\adminBundle\Entity\FotoBackgroundCampania $fotobackgroundcampania)
    {
        $this->fotobackgroundcampania[] = $fotobackgroundcampania;

        return $this;
    }

    /**
     * Remove fotobackgroundcampania
     *
     * @param \Mediadiv\adminBundle\Entity\FotoBackgroundCampania $fotobackgroundcampania
     */
    public function removeFotobackgroundcampanium(\Mediadiv\adminBundle\Entity\FotoBackgroundCampania $fotobackgroundcampania)
    {
        $this->fotobackgroundcampania->removeElement($fotobackgroundcampania);
    }

    /**
     * Get fotobackgroundcampania
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotobackgroundcampania()
    {
        return $this->fotobackgroundcampania;
    }

    /**
     * Add fotocampaniahome
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCampaniaHome $fotocampaniahome
     * @return Campania
     */
    public function addFotocampaniahome(\Mediadiv\adminBundle\Entity\FotoCampaniaHome $fotocampaniahome)
    {
        $this->fotocampaniahome[] = $fotocampaniahome;

        return $this;
    }

    /**
     * Remove fotocampaniahome
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCampaniaHome $fotocampaniahome
     */
    public function removeFotocampaniahome(\Mediadiv\adminBundle\Entity\FotoCampaniaHome $fotocampaniahome)
    {
        $this->fotocampaniahome->removeElement($fotocampaniahome);
    }

    /**
     * Get fotocampaniahome
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotocampaniahome()
    {
        return $this->fotocampaniahome;
    }
}
