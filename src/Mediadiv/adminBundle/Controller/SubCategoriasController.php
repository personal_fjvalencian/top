<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\adminBundle\Entity\SubCategorias;
use Mediadiv\adminBundle\Form\SubCategoriasType;

/**
 * SubCategorias controller.
 *
 */
class SubCategoriasController extends Controller {

    /**
     * Lists all SubCategorias entities.
     *
     */
    public function indexAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entities = $em->getRepository('MediadivadminBundle:SubCategorias')->findAll();

            return $this->render('MediadivadminBundle:SubCategorias:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new SubCategorias entity.
     *
     */
    public function createAction(Request $request) {


        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entity = new SubCategorias();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $entity->setFecha(new \DateTime());
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'subcategoria', 'Tu SubCategoría ah sido Guardada'
                );

                return $this->redirect($this->generateUrl('admin_subcategorias'));
            }

            return $this->render('MediadivadminBundle:SubCategorias:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a SubCategorias entity.
     *
     * @param SubCategorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubCategorias $entity) {
        $form = $this->createForm(new SubCategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_subcategorias_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Crear',
            'attr' => array('class' => 'btn btn-success')
                )
        );

        return $form;
    }

    /**
     * Displays a form to create a new SubCategorias entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = new SubCategorias();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:SubCategorias:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing SubCategorias entity.
     *
     */
    public function editAction($id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {


            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('MediadivadminBundle:SubCategorias')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubCategorias entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:SubCategorias:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a SubCategorias entity.
     *
     * @param SubCategorias $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SubCategorias $entity) {
        $form = $this->createForm(new SubCategoriasType(), $entity, array(
            'action' => $this->generateUrl('admin_subcategorias_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Editar',
            'attr' => array('class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Edits an existing SubCategorias entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        $entity = $em->getRepository('MediadivadminBundle:SubCategorias')->find($id);


        if ($admin) {
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SubCategorias entity.');
            }

            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'subcategoria', 'Tu SubCategoría ah sido Editada'
                );

                return $this->redirect($this->generateUrl('admin_subcategorias'));
            }

            return $this->render('MediadivadminBundle:SubCategorias:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function eliminarSubCategoriaAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'subcategoria', 'Tu SubCategoría ah sido Eliminada'
        );

        return $this->redirect($this->generateUrl('admin_subcategorias'));
    }

}
