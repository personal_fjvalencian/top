<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Mediadiv\adminBundle\Entity\SubCalce;
use Mediadiv\adminBundle\Form\SubCalceType;

/**
 * SubCalce controller.
 *
 */
class SubCalceController extends Controller
{

    /**
     * Lists all SubCalce entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediadivadminBundle:SubCalce')->findAll();

        return $this->render('MediadivadminBundle:SubCalce:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SubCalce entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SubCalce();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_subcalce_show', array('id' => $entity->getId())));
        }

        return $this->render('MediadivadminBundle:SubCalce:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SubCalce entity.
     *
     * @param SubCalce $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SubCalce $entity)
    {
        $form = $this->createForm(new SubCalceType(), $entity, array(
            'action' => $this->generateUrl('admin_subcalce_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SubCalce entity.
     *
     */
    public function newAction()
    {
        $entity = new SubCalce();
        $form   = $this->createCreateForm($entity);

        return $this->render('MediadivadminBundle:SubCalce:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SubCalce entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivadminBundle:SubCalce')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubCalce entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivadminBundle:SubCalce:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SubCalce entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivadminBundle:SubCalce')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubCalce entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MediadivadminBundle:SubCalce:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SubCalce entity.
    *
    * @param SubCalce $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SubCalce $entity)
    {
        $form = $this->createForm(new SubCalceType(), $entity, array(
            'action' => $this->generateUrl('admin_subcalce_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar' , 'attr' => array(
            'class' => 'btn btn-success')  ));

        return $form;
    }
    /**
     * Edits an existing SubCalce entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediadivadminBundle:SubCalce')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SubCalce entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_subcalce_edit', array('id' => $id)));
        }

        return $this->render('MediadivadminBundle:SubCalce:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SubCalce entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediadivadminBundle:SubCalce')->find($id);

          

            $em->remove($entity);
            $em->flush();
    

        return $this->redirect($this->generateUrl('admin_subcalce'));
    }

    /**
     * Creates a form to delete a SubCalce entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_subcalce_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Borrar'))
            ->getForm()
        ;
    }
}
