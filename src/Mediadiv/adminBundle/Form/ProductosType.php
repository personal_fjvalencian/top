<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductosType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre','text', array(
                 'label' => 'Nombre: ',
                    'required' => true,
                 'attr' => array(
                  'class' => 'form-control',
                  'id' => 'nombre'
                 )
                ))
            ->add('descripcion', 'textarea', 
                    array('label' => 'Descripción: ',
                      'required' => true,

                    'attr' => array(
                        'class' => 'jqte-test'

                    )
                ))
            ->add('unidadPorPack','text', array(
                 'label' => 'Unidad por Pack: ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))
            ->add('material', 'text', array(
                 'label' => 'Material : ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))
            ->add('talla', 'text', array(
                 'label' => 'Talla: ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))
            ->add('stock', 'text', array(
                 'label' => 'Stock: ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))
            ->add('cuidado', 'textarea', array(
                 'label' => 'Cuidado: ',
                 'attr' => array(
                  'class' => 'jqte-test'
                 )
                ))
            ->add('estado', 'text', array(
                  'label' => 'Estado: ',
                  'attr' => array(
                  'class' => 'form-control'
                 )
            ))

            ->add('subcategorias',
                  'entity',
                  array(
                      'class' => 'MediadivadminBundle:SubCategorias', 
                      'property' => 'nombre',
                      'attr' => array('class' => 'form-control'),
                      'label' => 'SubCategoria',
                      'required' => false,
                      'empty_value' => 'Seleccione Una subcategoria',
                      'empty_data' => null
                  )
            )
            ->add('Calce',
                  'entity',
                  array(
                      'class' => 'MediadivadminBundle:Calce', 
                      'property' => 'nombre',
                      'attr' => array('class' => 'form-control'),
                      'label' => 'Calce',
                      'required' => false,
                      'empty_value' => 'Seleccione un Calce',
                      'empty_data' => null
                  )
            )    
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\Productos'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_productos';
    }
}
