<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\adminBundle\Entity\FotoProducto;
use Mediadiv\adminBundle\Entity\FotoSliderProducto;
use Mediadiv\adminBundle\Entity\Productos;
use Mediadiv\adminBundle\Form\ProductosType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Productos controller.
 *
 */
class ProductosController extends Controller {

    /**
     * Lists all Productos entities.
     *
     */
    public function indexAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entities = $em->getRepository('MediadivadminBundle:Productos')->findAll();

            return $this->render('MediadivadminBundle:Productos:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Productos entity.
     *
     */
    public function createAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = new Productos();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entity->setFechaIngreso(new \DateTime());

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('productos_vistaUploadProducto'));
            }

            return $this->render('MediadivadminBundle:Productos:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Productos entity.
     *
     * @param Productos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Productos $entity) {
        $form = $this->createForm(new ProductosType(), $entity, array(
            'action' => $this->generateUrl('productos_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Guardar'));

        return $form;
    }

    /**
     * Displays a form to create a new Productos entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = new Productos();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Productos:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Productos entity.
     *
     */
    public function editAction($id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

      
        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Productos')->find($id);
            $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
            $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Productos entity.');
            }

            $editForm = $this->createEditForm($entity);


            return $this->render('MediadivadminBundle:Productos:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
                        'categoria' => $categoria,
                        'calce' => $calce
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }

 
    }

    /**
     * Creates a form to edit a Productos entity.
     *
     * @param Productos $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Productos $entity) {
        $form = $this->createForm(new ProductosType(), $entity, array(
            'action' => $this->generateUrl('productos_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar',
            'attr' => array('class' => 'btn btn-success')
                )
        );

        return $form;
    }

    /**
     * Edits an existing Productos entity.
     *
     */
    public function updateAction(Request $data, $id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $session = $this->getRequest()->getSession();
            $em = $this->getDoctrine()->getManager();

            $nombre = $data->request->get('nombreProducto');

            $descripcion = $data->request->get('descripcionProducto');
            $unidadesPorPack = $data->request->get('unidadesPackProducto');
            $materialProducto = $data->request->get('materialProducto');
            $talla = $data->request->get('tallaProducto');
            $cuidado = $data->request->get('cuidadoProducto');
            $stock = $data->request->get('stockProducto');
            $estado = $data->request->get('estadoProducto');

            $subcategoriaid = $data->request->get('SubCategoriaProducto');
            $calceId = $data->request->get('calceProducto');
            $subcalceId = $data->request->get('subcalce');

            if($calceId){

                   $calce = $em->getRepository('MediadivadminBundle:Calce')->findOneBy(array('id' => $calceId));

            }


            if($subcalceId){
            
            $subcalce = $em->getRepository('MediadivadminBundle:SubCalce')->findOneBy(array('id' => $subcalceId));

            }
            $subcategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneBy(array('id' => $subcategoriaid));
    

            $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $id));

            $producto->setNombre($nombre);
            $producto->setDescripcion($descripcion);
            $producto->setUnidadPorPack($unidadesPorPack);
            $producto->setMaterial($materialProducto);
            $producto->setTalla($talla);
            $producto->setCuidado($cuidado);
            $producto->setEstado($estado);
            $producto->setStock($stock);
            $producto->setSubcategorias($subcategoria);
            if( $subcalce ){

            $producto->setSubcalce($subcalce);

            }
            

            if($calce){

            $producto->setCalce($calce);

             }
            $producto->setFechaIngreso(new \DateTime());

            $em->merge($producto);
            $em->flush();


            $idProducto = $producto->getId();


                $this->get('session')->getFlashBag()->add(
                        'producto', 'Tu Producto ah sido Editado'
                );
            
            
            $session->set('idProducto', $idProducto);
            return $this->redirect($this->generateUrl('productos_vistaUploadProducto'));

            
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function eliminarProductoAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Productos')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();
        
            $this->get('session')->getFlashBag()->add(
                        'producto', 'Tu Producto ah sido Elimiado'
                );

        return $this->redirect($this->generateUrl('productos'));
    }

    public function vistaGuardarProductoAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
            $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();

            return $this->render(
                            'MediadivadminBundle:Productos:vistaGuardarProducto.html.twig', array(
                        'categoria' => $categoria,
                        'calce' => $calce
                            )
            );
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function selectSubCategoriasAction() {
        $request = $this->get('request');
        $id_categoria = $request->request->get('categoria_id');
        $em = $this->getDoctrine()->getManager();

        $SubCategorias = $em->getRepository('MediadivadminBundle:SubCategorias')->findBy(array('categorias' => $id_categoria));

        return $this->render('MediadivadminBundle:Default:selectCategorias.html.twig', array(
                    'data' => $SubCategorias,
        ));
    }

    public function guardarProductosAction(Request $data) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nombre = $data->request->get('nombreProducto');

        $descripcion = $data->request->get('descripcionProducto');
        $unidadesPorPack = $data->request->get('unidadesPackProducto');
        $materialProducto = $data->request->get('materialProducto');
        $talla = $data->request->get('tallaProducto');
        $cuidado = $data->request->get('cuidadoProducto');
        $stock = $data->request->get('stockProducto');
        $estado = $data->request->get('estadoProducto');

        $subcategoriaid = $data->request->get('SubCategoriaProducto');
        $calceId = $data->request->get('calceProducto');

         $subcalceId = $data->request->get('subcalce');
            if($subcalceId){
            
            $subcalce = $em->getRepository('MediadivadminBundle:SubCalce')->findOneBy(array('id' => $subcalceId));

            }
        $subcategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneBy(array('id' => $subcategoriaid));
        
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findOneBy(array('id' => $calceId ));


        $producto = new Productos();
        $producto->setNombre($nombre);
        $producto->setDescripcion($descripcion);
        $producto->setUnidadPorPack($unidadesPorPack);
        $producto->setMaterial($materialProducto);
        $producto->setTalla($talla);
        $producto->setCuidado($cuidado);
        $producto->setEstado($estado);
        $producto->setStock($stock);
        if($subcalce){

         $producto->setSubcalce($subcalce);   
        }
        $producto->setSubcategorias($subcategoria);
        $producto->setCalce($calce);
        $producto->setFechaIngreso(new \DateTime());

        $em->persist($producto);

        $em->flush($producto);

        


        $idProducto = $producto->getId();


        $session->set('idProducto', $idProducto);
        
        
            $this->get('session')->getFlashBag()->add(
                        'producto', 'Tu Producto ah sido Guardado'
                );
        
        return $this->redirect($this->generateUrl('productos_vistaUploadProducto'));
    }

    public function vistaUploadProductoAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $idProducto = $session->get('idProducto');

            return $this->render('MediadivadminBundle:Productos:vistaUploadProducto.html.twig', array(
                        'idProducto' => $idProducto
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function uploadProductoAction() {
        $session = $this->getRequest()->getSession();


        $id = $session->get('idProducto');

        $idProducto = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'producto/' . $idProducto . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'producto/' . $idProducto . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();
            $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $idProducto));

            $fotoProducto = new FotoProducto();

            $fotoProducto->setFechaIngreso(New \DateTime());
            $fotoProducto->setProductos($producto);
            $fotoProducto->setUrl($fileName);
            $em->persist($fotoProducto);
            $em->flush();


            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

    public function uploadSliderProductoAction() {
        $session = $this->getRequest()->getSession();


        $id = $session->get('idProducto');

        $idProducto = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'productoSlider/' . $idProducto . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'productoSlider/' . $idProducto . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();
            $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $idProducto));

            $fotoSliderProducto = new FotoSliderProducto();

            $fotoSliderProducto->setFechaIngreso(New \DateTime());
            $fotoSliderProducto->setProductos($producto);
            $fotoSliderProducto->setUrl($fileName);
            $em->persist($fotoSliderProducto);
            $em->flush();


            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

    public function elminaFotoProductoAction(Request $request) {
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();

        $fotoProducto = $em->getRepository('MediadivadminBundle:FotoProducto')->find($id);

        $em->remove($fotoProducto);
        $em->flush();

        return new Response('eliminado');
    }




}
