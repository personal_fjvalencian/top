<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotoCalce
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\adminBundle\Entity\FotoCalceRepository")
 */
class FotoCalce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */

    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="datetime")
     */
    private $fechaIngreso;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Calce", inversedBy="fotoproducto")
     * @ORM\JoinColumn(name="calce_id", referencedColumnName="id")
     *
     */


    private $calce;


    


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FotoCalce
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return FotoCalce
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set calce
     *
     * @param \Mediadiv\adminBundle\Entity\Calce $calce
     * @return FotoCalce
     */
    public function setCalce(\Mediadiv\adminBundle\Entity\Calce $calce = null)
    {
        $this->calce = $calce;

        return $this;
    }

    /**
     * Get calce
     *
     * @return \Mediadiv\adminBundle\Entity\Calce 
     */
    public function getCalce()
    {
        return $this->calce;
    }
}
