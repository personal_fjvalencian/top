<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tiendas
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tiendas {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sucursal", type="string", length=255)
     */
    private $sucursal;

    /**
     * @var string
     *
     * @ORM\Column(name="direccion", type="string", length=255, nullable=true)
     */
    private $direccion;


      /**
     * @var string
     *
     * @ORM\Column(name="horarios", type="string", length=255, nullable=true)
     */

    private $horarios;



      /**
     * @var string
     *
     * @ORM\Column(name="telefonos", type="string", length=255, nullable=true)
     */


    private $telefonos;


        /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */

    private $email;




    /**
     * @var string
     *
     * @ORM\Column(name="tumb", type="string", length=255, nullable=true)
     */


    private $tumb;




    /**
     * @var string
     *
     * @ORM\Column(name="url1", type="string", length=255, nullable=true)
     */
    private $url1;

    /**
     * @var string
     *
     * @ORM\Column(name="url2", type="string", length=255, nullable=true)
     */
    private $url2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="datetime", nullable=true)
     */
    private $fechaIngreso;

    /**
     * @var float
     *
     * @ORM\Column(name="latitud", type="float", length=255, nullable=true)
     */
    private $latitud;

    /**
     * @var float
     *
     * @ORM\Column(name="longitud", type="float", length=255, nullable=true)
     */
    private $longitud;


   

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sucursal
     *
     * @param string $sucursal
     * @return Tiendas
     */
    public function setSucursal($sucursal)
    {
        $this->sucursal = $sucursal;

        return $this;
    }

    /**
     * Get sucursal
     *
     * @return string 
     */
    public function getSucursal()
    {
        return $this->sucursal;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Tiendas
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set horarios
     *
     * @param string $horarios
     * @return Tiendas
     */
    public function setHorarios($horarios)
    {
        $this->horarios = $horarios;

        return $this;
    }

    /**
     * Get horarios
     *
     * @return string 
     */
    public function getHorarios()
    {
        return $this->horarios;
    }

    /**
     * Set telefonos
     *
     * @param string $telefonos
     * @return Tiendas
     */
    public function setTelefonos($telefonos)
    {
        $this->telefonos = $telefonos;

        return $this;
    }

    /**
     * Get telefonos
     *
     * @return string 
     */
    public function getTelefonos()
    {
        return $this->telefonos;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Tiendas
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set tumb
     *
     * @param string $tumb
     * @return Tiendas
     */
    public function setTumb($tumb)
    {
        $this->tumb = $tumb;

        return $this;
    }

    /**
     * Get tumb
     *
     * @return string 
     */
    public function getTumb()
    {
        return $this->tumb;
    }

    /**
     * Set url1
     *
     * @param string $url1
     * @return Tiendas
     */
    public function setUrl1($url1)
    {
        $this->url1 = $url1;

        return $this;
    }

    /**
     * Get url1
     *
     * @return string 
     */
    public function getUrl1()
    {
        return $this->url1;
    }

    /**
     * Set url2
     *
     * @param string $url2
     * @return Tiendas
     */
    public function setUrl2($url2)
    {
        $this->url2 = $url2;

        return $this;
    }

    /**
     * Get url2
     *
     * @return string 
     */
    public function getUrl2()
    {
        return $this->url2;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Tiendas
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set latitud
     *
     * @param float $latitud
     * @return Tiendas
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return float 
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     * @return Tiendas
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float 
     */
    public function getLongitud()
    {
        return $this->longitud;
    }
}
