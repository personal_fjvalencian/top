<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Linea
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\adminBundle\Entity\LineaRepository")
 */
class Linea {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="novedad", type="string", length=255)
     */
    private $novedad;

    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\Productos", mappedBy="linea" , cascade={"remove"})
     */
    private $productos;

   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Linea
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set novedad
     *
     * @param string $novedad
     * @return Linea
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return string 
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Add productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     * @return Linea
     */
    public function addProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     */
    public function removeProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
