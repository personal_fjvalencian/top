<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SubCategorias
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Mediadiv\adminBundle\Entity\SubCategoriasRepository")
 */
class SubCategorias
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */


  private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="novedad", type="string", length=255)
     */
    private $novedad;

    /**
     * @var string
     *
     * @ORM\Column(name="fecha", type="date")
     */
    private $fecha;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Tag", inversedBy="subcategorias" )
     * @ORM\JoinColumn(name="tag_id", referencedColumnName="id" , nullable=true ) 
     */




    private $tag;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Categorias", inversedBy="subcategorias" )
     * @ORM\JoinColumn(name="categorias_id", referencedColumnName="id")
     */

    private $categorias;


    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\Productos", mappedBy="subcategorias" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})  
     */

    private $productos;





 
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return SubCategorias
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set novedad
     *
     * @param string $novedad
     * @return SubCategorias
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return string 
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return SubCategorias
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set tag
     *
     * @param \Mediadiv\adminBundle\Entity\Tag $tag
     * @return SubCategorias
     */
    public function setTag(\Mediadiv\adminBundle\Entity\Tag $tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return \Mediadiv\adminBundle\Entity\Tag 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set categorias
     *
     * @param \Mediadiv\adminBundle\Entity\Categorias $categorias
     * @return SubCategorias
     */
    public function setCategorias(\Mediadiv\adminBundle\Entity\Categorias $categorias = null)
    {
        $this->categorias = $categorias;

        return $this;
    }

    /**
     * Get categorias
     *
     * @return \Mediadiv\adminBundle\Entity\Categorias 
     */
    public function getCategorias()
    {
        return $this->categorias;
    }

    /**
     * Add productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     * @return SubCategorias
     */
    public function addProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     */
    public function removeProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
