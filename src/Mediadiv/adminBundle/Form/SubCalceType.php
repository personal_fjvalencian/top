<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SubCalceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
         

            ->add('nombre', 'text', array(
                    'label' => 'Nombre: ',
                    'attr' => array(
                        'class' => 'form-control'
                    )



                ))
             ->add('calce',
                  'entity',
                   array(
                       'class' => 'MediadivadminBundle:Calce',
                       'property' => 'nombre',
                       'attr' => array('class' => 'form-control'),
                       'label' => 'Calce',
                       'required' => false,
                       'empty_value' => 'Selecciona el Calce',
                       'empty_data' => null,
                   )
                )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\SubCalce'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_subcalce';
    }
}
