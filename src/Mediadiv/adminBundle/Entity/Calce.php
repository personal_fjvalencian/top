<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calce
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Calce
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;


    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */


    private $descripcion;

   
  
    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\Productos", mappedBy="calce" , cascade={"remove"})
     * @ORM\OrderBy({"id" = "desc"})
     */

    private $productos;


      /**
     * @var string
     *
     * @ORM\Column(name="novedad", type="string", length=255)
     */


    private $novedad;







    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoCalce", mappedBy="calce" , cascade={"remove"})
     */
    private $fotocalce;



    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\SubCalce", mappedBy="calce" , cascade={"remove"})
     */

    private $subcalce;




    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fotocalce = new \Doctrine\Common\Collections\ArrayCollection();
        $this->subcalce = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Calce
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Calce
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set novedad
     *
     * @param string $novedad
     * @return Calce
     */
    public function setNovedad($novedad)
    {
        $this->novedad = $novedad;

        return $this;
    }

    /**
     * Get novedad
     *
     * @return string 
     */
    public function getNovedad()
    {
        return $this->novedad;
    }

    /**
     * Add productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     * @return Calce
     */
    public function addProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos[] = $productos;

        return $this;
    }

    /**
     * Remove productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     */
    public function removeProducto(\Mediadiv\adminBundle\Entity\Productos $productos)
    {
        $this->productos->removeElement($productos);
    }

    /**
     * Get productos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductos()
    {
        return $this->productos;
    }

    /**
     * Add fotocalce
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCalce $fotocalce
     * @return Calce
     */
    public function addFotocalce(\Mediadiv\adminBundle\Entity\FotoCalce $fotocalce)
    {
        $this->fotocalce[] = $fotocalce;

        return $this;
    }

    /**
     * Remove fotocalce
     *
     * @param \Mediadiv\adminBundle\Entity\FotoCalce $fotocalce
     */
    public function removeFotocalce(\Mediadiv\adminBundle\Entity\FotoCalce $fotocalce)
    {
        $this->fotocalce->removeElement($fotocalce);
    }

    /**
     * Get fotocalce
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotocalce()
    {
        return $this->fotocalce;
    }

    /**
     * Add subcalce
     *
     * @param \Mediadiv\adminBundle\Entity\SubCalce $subcalce
     * @return Calce
     */
    public function addSubcalce(\Mediadiv\adminBundle\Entity\SubCalce $subcalce)
    {
        $this->subcalce[] = $subcalce;

        return $this;
    }

    /**
     * Remove subcalce
     *
     * @param \Mediadiv\adminBundle\Entity\SubCalce $subcalce
     */
    public function removeSubcalce(\Mediadiv\adminBundle\Entity\SubCalce $subcalce)
    {
        $this->subcalce->removeElement($subcalce);
    }

    /**
     * Get subcalce
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSubcalce()
    {
        return $this->subcalce;
    }
}
