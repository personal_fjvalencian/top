<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CampaniaType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nombre', 'text', array(
                    'label' => 'Nombre : ',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))
                ->add('descripcion', 'textarea', array(
                    'label' => 'Descripción : ',
                    'attr' => array(
                        'class' => 'form-control  jqte-test'
                    )
                ))
                ->add('titulo', 'text', array(
                    'label' => 'Titulo : ',
                    'attr' => array(
                        'class' => 'form-control'
                    )
                ))

    ->add('tipo', 'choice', array(
    'label' => 'select some colors',
    'multiple' => false,
    'choices' => array( '' => 'Seleccion Tipo Video o fotos' , 'video' => 'video', 'fotografias' => 'fotografias'),
    'attr' => array('class' => 'form-control' , 'id' => 'tipoCampana'  )

       ))


  ->add('url', 'text', array(
                    'label' => 'Url : ',
                      'required' => false,
                    'attr' => array(
                        'class' => 'form-control',

                      
                    )
                ))


    ;


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\Campania'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'mediadiv_adminbundle_campania';
    }

}
