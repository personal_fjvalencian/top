<?php

namespace Mediadiv\CarritosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {	



    	$em = $this->getDoctrine()->getManager();
    	
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
    	$categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $slider = $em->getRepository('MediadivadminBundle:Slider')->findBy(array(),array('orden' => 'desc'));
        $calce = $em->getRepository('MediadivadminBundle:Calce');
        $query = $calce->createQueryBuilder('p')->orderBy('p.id', 'asc')->setMaxResults(6)->getQuery();
        $calce = $query->getResult();


        return $this->render('MediadivCarritosBundle:Default:index.html.twig'

        	,
                array(
                    'productos'  => $productos,
                    'categorias' => $categorias, 
                    'calceMenu' => $calceMenu ,

                    'calce' => $calce,
                    'lineas' => $lineas,
                    'campania' => $campania,
                    'slider' => $slider
                ));

    }



//  primer listado calce
     public function ListarCalceAction($tipo , $id) {



        $em = $this->getDoctrine()->getManager();

        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findby(array(),array('id' => 'asc'));
      
        if($tipo == 'calce'){

        $entidad = $em->getRepository('MediadivadminBundle:Calce')->findOneBy(array('id' => $id ));    
                 $contador = count($entidad);
        return $this->render('MediadivCarritosBundle:Default:listarCalce.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'entidad' => $entidad,
                    'contador' => $contador
     

                    )
                );

        }

        if($tipo == 'Subcalce'){

        $entidad = $em->getRepository('MediadivadminBundle:SubCalce')->findOneBy(array('id' => $id ));
        $contador = count($entidad);

        return $this->render('MediadivCarritosBundle:Default:listarSubCalce.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'entidad' => $entidad,
                    'contador' => $contador
                    )
                );

    }
    }


    // producto solo poner condicion 


     public function productosSoloAction($id) {
        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $id));
        $subCategoriaId =  $producto->getSubcategorias()->getId();

        $subCategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneby(array('id' => $subCategoriaId));



        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $tiendas = $em->getRepository('MediadivadminBundle:Tiendas')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        return $this->render('MediadivCarritosBundle:Default:productoDetalle.html.twig', 
            
        array(
            'producto' => $producto,
            'tiendas' => $tiendas, 
            'productos'=>$productos,
            'categorias' => $categoria,
            'lineas' => $lineas,
            'calceMenu' => $calceMenu,
            'campania' => $campania,
            'subCategoria' => $subCategoria
        ));
    }



     public function ShopCategoriaAction($id){
        $em = $this->getDoctrine()->getManager();
        $categoriaFinal = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categoriaListar = $em->getRepository('MediadivadminBundle:Categorias')->findBy(array('id' => $id) , array('id' => 'desc'));
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        
        return $this->render('MediadivCarritosBundle:Default:categoria.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias , 
                    'calceMenu' => $calceMenu ,            
                    'campania' => $campania,
                    'categoriaFinal' => $categoriaFinal,
                    'categoriaListar' => $categoriaListar
                ));
    }



    public function ShopsubCategoriasAction($id) {

        $em = $this->getDoctrine()->getManager();
        
        $subcategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneby(array('id' => $id));
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categoriaListar = $em->getRepository('MediadivadminBundle:Categorias')->findBy(array('id' => $id));
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        
        return $this->render('MediadivCarritosBundle:Default:subCategoria.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'subcategoria' => $subcategoria,
                    'categoriaListar' => $categoriaListar
                    )
                );
    

    }
    
    

public function listarItemAction(){


          $em = $this->getDoctrine()->getManager();
        
     
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
      
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
     
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();


        return $this->render('MediadivCarritosBundle:Default:listadoBolsa.html.twig',
               array(
                   
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                   
                    )
                );

}


 

public function micuentaAction(){


          $em = $this->getDoctrine()->getManager();
        
     
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
      
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
     
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();


        return $this->render('MediadivCarritosBundle:Default:micuenta.html.twig',
               array(
                   
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                   
                    )
                );

}


public function ordenAction(){


          $em = $this->getDoctrine()->getManager();
        
     
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
      
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
     
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();


        return $this->render('MediadivCarritosBundle:Default:orden.html.twig',
               array(
                   
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                   
                    )
                );

}


public function despachoAction(){


          $em = $this->getDoctrine()->getManager();
        
     
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
      
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
     
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();


        return $this->render('MediadivCarritosBundle:Default:despacho.html.twig',
               array(
                   
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                   
                    )
                );

}


}
