<?php

namespace Mediadiv\paginasBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
    	$categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $slider = $em->getRepository('MediadivadminBundle:Slider')->findBy(array(),array('orden' => 'asc'));
        $calce = $em->getRepository('MediadivadminBundle:Calce');
        $query = $calce->createQueryBuilder('p')->orderBy('p.id', 'asc')->setMaxResults(6)->getQuery();
        $calce = $query->getResult();

        return $this->render('MediadivpaginasBundle:Default:index.html.twig' ,
                array(
                    'productos'  => $productos,
                    'categorias' => $categorias, 
                    'calceMenu' => $calceMenu ,

                    'calce' => $calce,
                    'lineas' => $lineas,
                    'campania' => $campania,
                    'slider' => $slider
                ));
    }
   


// del menu superior

       public function ListarCalceAction($tipo , $id) {



        $em = $this->getDoctrine()->getManager();

        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findby(array(),array('id' => 'asc'));
      
        if($tipo == 'calce'){

        $entidad = $em->getRepository('MediadivadminBundle:Calce')->findOneBy(array('id' => $id ));    
                 $contador = count($entidad);
        return $this->render('MediadivpaginasBundle:Default:listarCalce.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'entidad' => $entidad,
                    'contador' => $contador
     

                    )
                );

        }

        if($tipo == 'Subcalce'){

        $entidad = $em->getRepository('MediadivadminBundle:SubCalce')->findOneBy(array('id' => $id ));
        $contador = count($entidad);

        return $this->render('MediadivpaginasBundle:Default:listarSubCalce.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'entidad' => $entidad,
                    'contador' => $contador
                    )
                );

    }
    }


// de los iconos index 
    public function CalceListarAction($id){


        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $calceDetalle =  $em->getRepository('MediadivadminBundle:Calce')->findOneby(array('id' => $id ));
        $productosCalce  =  $em->getRepository('MediadivadminBundle:Productos')->findBy(array('calce' => $calceDetalle) , array('id' => 'desc'));
         $contador = count($productosCalce);
      




        
        return $this->render('MediadivpaginasBundle:Default:calceListado.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'calceDetalle' =>  $calceDetalle,
                    'contador' => $contador,
                    'productosCalce' => $productosCalce,
 
              
                ));


    }


    public function CategoriaAction($id){
        $em = $this->getDoctrine()->getManager();
        $categoriaFinal = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $calce = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categoriaListar = $em->getRepository('MediadivadminBundle:Categorias')->findBy(array('id' => $id) , array('id' => 'desc'));
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        
        return $this->render('MediadivpaginasBundle:Default:categoria.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias , 
                    'calceMenu' => $calceMenu ,            
                    'campania' => $campania,
                    'categoriaFinal' => $categoriaFinal,
                    'categoriaListar' => $categoriaListar
                ));
    }
    
    
    public function subCategoriasAction($id) {

        $em = $this->getDoctrine()->getManager();
        
        $subcategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneby(array('id' => $id));
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos = $em->getRepository('MediadivadminBundle:Categorias')->findOneby(array('id' => $id));
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categoriaListar = $em->getRepository('MediadivadminBundle:Categorias')->findBy(array('id' => $id));
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        
        return $this->render('MediadivpaginasBundle:Default:subCategoria.html.twig' ,
               array(
                    'productos'  => $productos ,
                    'categorias' => $categorias ,             
                    'campania' => $campania,
                    'calceMenu' => $calceMenu,
                    'subcategoria' => $subcategoria,
                    'categoriaListar' => $categoriaListar
                    )
                );
    }
    
    public function campaniaAction() {
        $em = $this->getDoctrine()->getManager();
        
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categorias = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $calceMenu  = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        
        return $this->render('MediadivpaginasBundle:Default:campania.html.twig', 
                array(
                    'productos' => $productos,
                    'lineas' => $lineas,
                    'categorias' => $categorias,
                    'calceMenu' => $calceMenu,
                    'campania' => $campania                    
                ));
    }
    public function vistaCampaniaAction($id) {
        $em = $this->getDoctrine()->getManager();
        $campanias_id = $em->getRepository('MediadivadminBundle:Campania')->findOneBy(array('id' => $id));
        $campanias = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        
        return $this->render('MediadivpaginasBundle:Default:campaniaIndividual.html.twig', 
                array(
                    'productos' => $productos,
                    'lineas' => $lineas,
                    'categorias' => $categoria,
                    'campania' => $campanias,
                    'calceMenu' => $calceMenu,
                    'campaniaid' => $campanias_id
                    
                ));
    }
    public function tiendasAction() {
        $em = $this->getDoctrine()->getManager();
        
        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $tiendas = $em->getRepository('MediadivadminBundle:Tiendas')->findby(array(),array('id' => 'desc'));
        
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        return $this->render('MediadivpaginasBundle:Default:nuestrasTiendas.html.twig', array(
            'tiendas' => $tiendas, 
            'productos'=>$productos,
            'categorias' => $categoria,
            'lineas' => $lineas,
            'calceMenu' => $calceMenu,
            'campania' => $campania
        ));
    }
    
    public function contactoAction() {
        $em = $this->getDoctrine()->getManager();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        

        return $this->render('MediadivpaginasBundle:Default:contacto.html.twig', 
                array(
                    'campania' => $campania, 
                    'productos'=>$productos,
                    'lineas' => $lineas,
                     'categorias' => $categoria,
                     'calceMenu' => $calceMenu
                ));
    }
    
     public function productosAction() {
        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $id));
        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $tiendas = $em->getRepository('MediadivadminBundle:Tiendas')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        

        return $this->render('MediadivpaginasBundle:Default:productos.html.twig', 
            
        array(
            'producto' => $producto,
            'tiendas' => $tiendas, 
            'productos'=>$productos,
            'categorias' => $categoria,
            'lineas' => $lineas,
            'calceMenu' => $calceMenu,
            'campania' => $campania
        ));
    }


    public function detalleTiendaAction($id){


        $em = $this->getDoctrine()->getManager();
        $tienda  = $em->getRepository('MediadivadminBundle:Tiendas')->findOneby(array('id' => $id ));
        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $tiendas = $em->getRepository('MediadivadminBundle:Tiendas')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        return $this->render('MediadivpaginasBundle:Default:tiendaDetalle.html.twig',
            
        array(
            'tienda' => $tienda,
            'tiendas' => $tiendas, 
            'productos'=>$productos,
            'categorias' => $categoria,
            'lineas' => $lineas,
            'calceMenu' => $calceMenu,
            'campania' => $campania
        ));



    }
    
    
    public function productosSoloAction($id) {
        $em = $this->getDoctrine()->getManager();
        $producto = $em->getRepository('MediadivadminBundle:Productos')->findOneBy(array('id' => $id));
        $subCategoriaId =  $producto->getSubcategorias()->getId();

        $subCategoria = $em->getRepository('MediadivadminBundle:SubCategorias')->findOneby(array('id' => $subCategoriaId));



        $categoria = $em->getRepository('MediadivadminBundle:Categorias')->findAll();
        $productos =  $em->getRepository('MediadivadminBundle:Productos')->findAll();
        $campania = $em->getRepository('MediadivadminBundle:Campania')->findAll();
        $tiendas = $em->getRepository('MediadivadminBundle:Tiendas')->findAll();
        $lineas =  $em->getRepository('MediadivadminBundle:Linea')->findAll();
        $calceMenu = $em->getRepository('MediadivadminBundle:Calce')->findAll();
        return $this->render('MediadivpaginasBundle:Default:productoDetalle.html.twig', 
            
        array(
            'producto' => $producto,
            'tiendas' => $tiendas, 
            'productos'=>$productos,
            'categorias' => $categoria,
            'lineas' => $lineas,
            'calceMenu' => $calceMenu,
            'campania' => $campania,
            'subCategoria' => $subCategoria
        ));
    }
    
    public function enviarCorreoAction(Request $request){
        
        $nombre = $request->request->get('name');
        $telefono = $request->request->get('phone');
        $email = $request->request->get('email');
        $asunto =  $request->request->get('subject');
        $mensaje = $request->request->get('comments');
        
        $message = \Swift_Message::newInstance()
                    ->setSubject('Contacto web')
                    ->setFrom('giorgosbarkos@gmail.com')
                    ->setTo('giorgosbarkos@gmail.com')
                    ->setContentType("text/html")
                    ->setBody(
                    $this->renderView(
                        'MediadivpaginasBundle:Default:enviaCorreo.html.twig',
                        array(
                            'nombre' => $nombre,
                            'telefono' => $telefono,
                            'correo' => $email, 
                            'asunto'=> $asunto, 
                            'mensaje' => $mensaje
                            )
                        )
                    );
        $this->get('mailer')->send($message);




         $message = \Swift_Message::newInstance()
                    ->setSubject('Contacto web')
                    ->setFrom('info@topwear.cl')
                    ->setTo('info@topwear.cl')
                    ->setContentType("text/html")
                    ->setBody(
                    $this->renderView(
                        'MediadivpaginasBundle:Default:enviaCorreo.html.twig',
                        array(
                            'nombre' => $nombre,
                            'telefono' => $telefono,
                            'correo' => $email, 
                            'asunto'=> $asunto, 
                            'mensaje' => $mensaje
                            )
                        )
                    );
        $this->get('mailer')->send($message);
        $this->get('session')->getFlashBag()->add('contacto', 'Email enviado Correctamente');
    
        return $this->redirect($this->generateUrl('home_contacto'));
        
    }
}
