<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TiendasType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sucursal', 'text', array('attr' => array('class' => 'form-control')))
            ->add('direccion', 'text', array('attr' => array('class' => 'form-control')))
            ->add('horarios', 'text', array('attr' => array('class' => 'form-control')))
            ->add('telefonos', 'text', array('attr' => array('class' => 'form-control')))
            ->add('email', 'text', array('attr' => array('class' => 'form-control')))

            ->add('latitud', 'text', array('attr' => array('class' => 'form-control')))
            ->add('longitud', 'text', array('attr' => array('class' => 'form-control')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\Tiendas'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_tiendas';
    }
}
