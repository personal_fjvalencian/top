<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\DateTime;
use Mediadiv\adminBundle\Entity\Slider;
use Mediadiv\adminBundle\Entity\FotoSlider;
use Mediadiv\adminBundle\Form\SliderType;

/**
 * Slider controller.
 *
 */
class SliderController extends Controller {

    /**
     * Lists all Slider entities.
     *
     */
    public function indexAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entities = $em->getRepository('MediadivadminBundle:Slider')->findAll();

            return $this->render('MediadivadminBundle:Slider:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Slider entity.
     *
     */
    public function createAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = new Slider();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $entity->setFechaIngreso(new \DateTime());
                $em->persist($entity);
                $em->flush();

                $idSlider = $entity->getId();
                $session->set('idSlider', $idSlider);

                $this->get('session')->getFlashBag()->add(
                        'slider', 'Tu Slider ah sido Guardado'
                );

                return $this->redirect($this->generateUrl('slider_vistaUploadSlider'));
            }

            return $this->render('MediadivadminBundle:Slider:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Slider entity.
     *
     * @param Slider $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Slider $entity) {
        $form = $this->createForm(new SliderType(), $entity, array(
            'action' => $this->generateUrl('slider_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Subir foto', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Slider entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {


            $entity = new Slider();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Slider:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Slider entity.
     *
     */
    public function editAction($id) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entity = $em->getRepository('MediadivadminBundle:Slider')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Slider entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Slider:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Slider entity.
     *
     * @param Slider $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Slider $entity) {
        $form = $this->createForm(new SliderType(), $entity, array(
            'action' => $this->generateUrl('slider_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Editar', 'attr' => array('class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Edits an existing Slider entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Slider')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Slider entity.');
            }

            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $idSlider = $entity->getId();
                $session->set('idSlider', $idSlider);


                $this->get('session')->getFlashBag()->add(
                        'slider', 'Tu Slider ah sido Editado'
                );


                return $this->redirect($this->generateUrl('slider_vistaUploadSlider'));
            }

            return $this->render('MediadivadminBundle:Slider:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function updateListaSliderAction(Request $request) {
        $data = $request->request->all();

        $params = array();
        $content = $this->get('request')->getContent();
        $orden = $request->request->get('nuevoOrden');
        $elementos = count($orden);
        $em = $this->getDoctrine()->getManager();

        for ($i = 0; $i < $elementos; $i++) {

            $lista = $em->getRepository('MediadivadminBundle:Slider')->findOneBy(array('id' => $orden[$i]['id']));
            $lista->setOrden($orden[$i]['orden']);
            $em->persist($lista);
            $em->flush();

            $this->get('session')->getFlashBag()->add(
                    'tiposeccion', 'la lista se ha actualizado'
            );
        }



        return new Response(100);
    }

    public function vistaUploadSliderAction() {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $idSlider = $session->get('idSlider');
            return $this->render('MediadivadminBundle:Slider:vistaUploadSlider.html.twig', array('idSlider' => $idSlider));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function uploadSliderAction() {
        $session = $this->getRequest()->getSession();


        $id = $session->get('idSlider');

        $idSlider = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'slider/' . $idSlider . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'slider/' . $idSlider . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);



            $em = $this->getDoctrine()->getManager();

            $slider = $em->getRepository('MediadivadminBundle:Slider')->findOneBy(array('id' => $idSlider));
            $fotoSlider = new FotoSlider();

            $fotoSlider->setFechaIngreso(new \DateTime());
            $fotoSlider->setSlider($slider);
            $fotoSlider->setUrl($fileName);

            $em->persist($fotoSlider);
            $em->flush();

            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

    public function elminaFotoSliderAction(Request $request) {
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();

        $fotoslider = $em->getRepository('MediadivadminBundle:FotoSlider')->find($id);

        $em->remove($fotoslider);
        $em->flush();

        return new Response('200');
    }

    public function eliminarSliderAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Slider')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();


        $this->get('session')->getFlashBag()->add(
                'slider', 'Tu Slider ah sido Eliminado'
        );


        return $this->redirect($this->generateUrl('slider'));
    }

}
