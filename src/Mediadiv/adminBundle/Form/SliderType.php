<?php

namespace Mediadiv\adminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SliderType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo','text', array(
                 'label' => 'Título: ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))

              ->add('url','text', array(
                 'label' => 'Url: ',
                 'attr' => array(
                  'class' => 'form-control'
                 )
                ))
            ->add('texto1', 'textarea', array(
                 'label' => 'Descripción: ',
                 'attr' => array(
                  'class' => 'form-control jqte-test'
                 )
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Mediadiv\adminBundle\Entity\Slider'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mediadiv_adminbundle_slider';
    }
}
