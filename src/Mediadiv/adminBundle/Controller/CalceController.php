<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Mediadiv\adminBundle\Entity\Calce;
use Mediadiv\adminBundle\Entity\FotoCalce;
use Mediadiv\adminBundle\Form\CalceType;

/**
 * Calce controller.
 *
 */
class CalceController extends Controller {

    /**
     * Lists all Calce entities.
     *
     */
    public function indexAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entities = $em->getRepository('MediadivadminBundle:Calce')->findAll();

            return $this->render('MediadivadminBundle:Calce:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Calce entity.
     *
     */
    public function createAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {
            $entity = new Calce();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $idCalce = $entity->getId();


                $session->set('idCalce', $idCalce);
                
                
                $this->get('session')->getFlashBag()->add(
                        'calce',
                        'Tu calce a sido guardado'
                );
                

                return $this->redirect($this->generateUrl('admin_vistaUploadFotoCalce'));
            }

            return $this->render('MediadivadminBundle:Calce:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Calce entity.
     *
     * @param Calce $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Calce $entity) {
        $form = $this->createForm(new CalceType(), $entity, array(
            'action' => $this->generateUrl('admin_calce_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array(
                'class' => 'btn btn-success')
        ));

        return $form;
    }

    /**
     * Displays a form to create a new Calce entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = new Calce();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Calce:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Calce entity.
     *
     */
    public function editAction($id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = $em->getRepository('MediadivadminBundle:Calce')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Calce entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Calce:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Calce entity.
     *
     * @param Calce $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Calce $entity) {
        $form = $this->createForm(new CalceType(), $entity, array(
            'action' => $this->generateUrl('admin_calce_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Siguiente',
            'attr' => array(
                'class' => 'btn btn-success'
            )
        ));

        return $form;
    }

    /**
     * Edits an existing Calce entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {

            $entity = $em->getRepository('MediadivadminBundle:Calce')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Calce entity.');
            }

            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $idCalce = $entity->getId();
                $session->set('idCalce', $idCalce);

                $this->get('session')->getFlashBag()->add(
                        'calce',
                        'Tu calce a sido Editado'
                );

                return $this->redirect($this->generateUrl('admin_vistaUploadFotoCalce'));
            }

            return $this->render('MediadivadminBundle:Calce:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));            
        }
    }

    public function eliminarCalceAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Calce')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        
         $this->get('session')->getFlashBag()->add(
                        'eliminacalce',
                        'Tu calce a sido Eliminado'
                );
        
        return $this->redirect($this->generateUrl('admin_calce'));
    }

    public function vistaUploadFotoCalceAction() {
        
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            
        $idCalce = $session->get('idCalce');

        return $this->render('MediadivadminBundle:Calce:uploadFotoCalce.html.twig', 
                array(
                    'idCalce' => $idCalce
                ));
        }else{
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));            
        }
    }

    public function uploadFotoCalceAction() {
        $session = $this->getRequest()->getSession();


        $id = $session->get('idCalce');

        $idCalce = $id;

        $fileName = ($_REQUEST["name"]);

        $targetDirectorio = 'calce/' . $idCalce . '/' . $fileName;
        if (file_exists($targetDirectorio)) {
            
        } else {
            //   mkdir('fotos/', 0777, true);
        }



        $targetDir = 'calce/' . $idCalce . '/';

        //$targetDir = 'uploads';

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600; // Temp file age in seconds

        @set_time_limit(5 * 60);


        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';


        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

        if ($chunks < 2 && file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b))
                $count++;

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . DIRECTORY_SEPARATOR . $fileName;


        if (!file_exists($targetDir))
            @mkdir($targetDir);


        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }
        }

        $uno = 'uno';




        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];


        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {

                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096))
                        fwrite($out, $buff);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');

                @fclose($in);
                @fclose($out);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (!$chunks || $chunk == $chunks - 1) {
            // Strip the temp .part suffix off
            rename("{$filePath}.part", $filePath);


            $em = $this->getDoctrine()->getManager();
            $calce = $em->getRepository('MediadivadminBundle:Calce')->findOneBy(array('id' => $idCalce));

            $fotoCalce = new FotoCalce();

            $fotoCalce->setFechaIngreso(new \DateTime());
            $fotoCalce->setUrl($fileName);
            $fotoCalce->setCalce($calce);

            $em->persist($calce);
            $em->persist($fotoCalce);
            $em->flush();
            
            

            $resp = '<script>
                        alert("Tu contenido se ha subido exitosamente");
                   </script>';
        }
        return new Response($resp);
    }

    public function elminaFotoCalceAction(Request $request) {
        $id = $request->request->get('recordToDelete');
        $em = $this->getDoctrine()->getManager();

        $fotoCalce = $em->getRepository('MediadivadminBundle:FotoCalce')->find($id);

        $em->remove($fotoCalce);
        $em->flush();

       
        
        return new Response('eliminado');
    }

}
