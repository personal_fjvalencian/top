<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Slider
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Slider
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;


        /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;


    /**
     * @var string
     *
     * @ORM\Column(name="texto1", type="text")
     */
    private $texto1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="orden", type="integer", nullable=true)
     */
    private $orden;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="date")
     */
    private $fechaIngreso;


    /**
     * @ORM\OneToMany(targetEntity="Mediadiv\adminBundle\Entity\FotoSlider", mappedBy="slider" , cascade={"remove"})
     */

    private $fotoslider;

    
   
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fotoslider = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Slider
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Slider
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set texto1
     *
     * @param string $texto1
     * @return Slider
     */
    public function setTexto1($texto1)
    {
        $this->texto1 = $texto1;

        return $this;
    }

    /**
     * Get texto1
     *
     * @return string 
     */
    public function getTexto1()
    {
        return $this->texto1;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     * @return Slider
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;

        return $this;
    }

    /**
     * Get orden
     *
     * @return integer 
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return Slider
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Add fotoslider
     *
     * @param \Mediadiv\adminBundle\Entity\FotoSlider $fotoslider
     * @return Slider
     */
    public function addFotoslider(\Mediadiv\adminBundle\Entity\FotoSlider $fotoslider)
    {
        $this->fotoslider[] = $fotoslider;

        return $this;
    }

    /**
     * Remove fotoslider
     *
     * @param \Mediadiv\adminBundle\Entity\FotoSlider $fotoslider
     */
    public function removeFotoslider(\Mediadiv\adminBundle\Entity\FotoSlider $fotoslider)
    {
        $this->fotoslider->removeElement($fotoslider);
    }

    /**
     * Get fotoslider
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFotoslider()
    {
        return $this->fotoslider;
    }
}
