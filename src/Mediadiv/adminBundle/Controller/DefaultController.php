<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Mediadiv\adminBundle\Entity\Admin;
use Mediadiv\adminBundle\Entity\Categorias;
use Mediadiv\adminBundle\Entity\SubCategorias;
use Mediadiv\adminBundle\Entity\Productos;
use Mediadiv\adminBundle\Entity\FotoProducto;
use Mediadiv\adminBundle\Entity\Campania;
use Mediadiv\adminBundle\Entity\FotoCampania;
use Mediadiv\adminBundle\Entity\Tiendas;
use Symfony\Component\HttpFoundation\Session\Session;



class DefaultController extends Controller
{
    #Vistas
    public function indexAction()
    {
        return $this->render('MediadivadminBundle:Default:index.html.twig');
    }
    
    public function principalAction() {
        return $this->render('MediadivadminBundle:Default:principal.html.twig');
    }
    
    #Acciones.


      public function selectCategoriasAction( ){
        $request = $this->get('request');
        $id_categoria = $request->request->get('categoria_id');
        $em = $this->getDoctrine()->getManager();

        $SubCategorias = $em->getRepository('MediadivadminBundle:SubCategorias')->findBy(array ('categorias'=> $id_categoria ));




           return $this->render('MediadivadminBundle:SubCategorias:selectCategorias.html.twig' , array (
           
            'data' => $SubCategorias,
          
        ));




    }

    public function selectSubCalceAction(){

        $request = $this->get('request');
        $id_categoria = $request->request->get('categoria_id');
        $em = $this->getDoctrine()->getManager();

        $SubCategorias = $em->getRepository('MediadivadminBundle:SubCalce')->findBy(array ('calce'=> $id_categoria ));




           return $this->render('MediadivadminBundle:SubCategorias:selectCategorias.html.twig' , array (
           
            'data' => $SubCategorias,
          
        ));

    }





    
    public function loginAction(Request $data ) {
        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();
        
        $usuario = $data->request->get('nusuario');
        $password = $data->request->get('contrasena');
        
        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(
                 array('usuario' => $usuario, 
                       'password' => $password
                 ));
        
        if($admin){
            $session->set('nusuario', $usuario);
            $session->set('contrasena', $password);
            return new Response('200');
            
        }else{
            return new Response('100');
        }
    }
    
    
    public function logoutAction() {
        $session = $this->getRequest()->getSession();
        $this->get('session')->clear();
        return $this->redirect($this->generateUrl('mediadivadmin_homepage'));  
    }
    
    
}
