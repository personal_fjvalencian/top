<?php

namespace Mediadiv\adminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Mediadiv\adminBundle\Entity\Linea;
use Mediadiv\adminBundle\Form\LineaType;

/**
 * Linea controller.
 *
 */
class LineaController extends Controller {

    /**
     * Lists all Linea entities.
     *
     */
    public function indexAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {


            $em = $this->getDoctrine()->getManager();

            $entities = $em->getRepository('MediadivadminBundle:Linea')->findAll();

            return $this->render('MediadivadminBundle:Linea:index.html.twig', array(
                        'entities' => $entities,
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a new Linea entity.
     *
     */
    public function createAction(Request $request) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {


            $entity = new Linea();
            $form = $this->createCreateForm($entity);
            $form->handleRequest($request);

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'linea', 'Tu Linea a sido Guardada'
                );


                return $this->redirect($this->generateUrl('admin_linea'));
            }

            return $this->render('MediadivadminBundle:Linea:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to create a Linea entity.
     *
     * @param Linea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Linea $entity) {


        $form = $this->createForm(new LineaType(), $entity, array(
            'action' => $this->generateUrl('admin_linea_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Crear', 'attr' => array(
                'class' => 'btn btn-success')));

        return $form;
    }

    /**
     * Displays a form to create a new Linea entity.
     *
     */
    public function newAction() {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {

            $entity = new Linea();
            $form = $this->createCreateForm($entity);

            return $this->render('MediadivadminBundle:Linea:new.html.twig', array(
                        'entity' => $entity,
                        'form' => $form->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Displays a form to edit an existing Linea entity.
     *
     */
    public function editAction($id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));

        if ($admin) {
            $entity = $em->getRepository('MediadivadminBundle:Linea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Linea entity.');
            }

            $editForm = $this->createEditForm($entity);

            return $this->render('MediadivadminBundle:Linea:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    /**
     * Creates a form to edit a Linea entity.
     *
     * @param Linea $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Linea $entity) {
        $form = $this->createForm(new LineaType(), $entity, array(
            'action' => $this->generateUrl(
                    'admin_linea_update', array(
                'id' => $entity->getId()
            )),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array(
            'label' => 'Editar',
            'attr' => array(
                'class' => 'btn btn-success'
            )
                )
        );

        return $form;
    }

    /**
     * Edits an existing Linea entity.
     *
     */
    public function updateAction(Request $request, $id) {

        $session = $this->getRequest()->getSession();
        $em = $this->getDoctrine()->getManager();

        $nusuario = $session->get('nusuario');
        $contrasena = $session->get('contrasena');

        $admin = $em->getRepository('MediadivadminBundle:Admin')->findOneBy(array('usuario' => $nusuario, 'password' => $contrasena));


        if ($admin) {


            $entity = $em->getRepository('MediadivadminBundle:Linea')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Linea entity.');
            }

            $editForm = $this->createEditForm($entity);
            $editForm->handleRequest($request);

            if ($editForm->isValid()) {
                $em->flush();

                $this->get('session')->getFlashBag()->add(
                        'linea', 'Tu Linea a sido Editada'
                );


                return $this->redirect($this->generateUrl('admin_linea'));
            }

            return $this->render('MediadivadminBundle:Linea:edit.html.twig', array(
                        'entity' => $entity,
                        'edit_form' => $editForm->createView(),
            ));
        } else {
            $this->get('session')->clear();
            return $this->redirect($this->generateUrl('mediadivadmin_homepage'));
        }
    }

    public function eliminarLineaAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediadivadminBundle:Linea')->findOneby(array('id' => $id));

        $em->remove($entity);
        $em->flush();

        $this->get('session')->getFlashBag()->add(
                'linea', 'Tu Linea a sido Eliminada'
        );


        return $this->redirect($this->generateUrl('admin_linea'));
    }

}
