<?php

namespace Mediadiv\adminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FotoProducto
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FotoProducto {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaIngreso", type="datetime")
     */
    private $fechaIngreso;

    /**
     * @ORM\ManyToOne(targetEntity="Mediadiv\adminBundle\Entity\Productos", inversedBy="fotoproducto")
     * @ORM\JoinColumn(name="productos_id", referencedColumnName="id")
     *
     */
    private $productos;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return FotoProducto
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     * @return FotoProducto
     */
    public function setFechaIngreso($fechaIngreso)
    {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime 
     */
    public function getFechaIngreso()
    {
        return $this->fechaIngreso;
    }

    /**
     * Set productos
     *
     * @param \Mediadiv\adminBundle\Entity\Productos $productos
     * @return FotoProducto
     */
    public function setProductos(\Mediadiv\adminBundle\Entity\Productos $productos = null)
    {
        $this->productos = $productos;

        return $this;
    }

    /**
     * Get productos
     *
     * @return \Mediadiv\adminBundle\Entity\Productos 
     */
    public function getProductos()
    {
        return $this->productos;
    }
}
