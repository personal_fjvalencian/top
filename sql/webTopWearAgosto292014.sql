-- phpMyAdmin SQL Dump
-- version 4.2.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-08-2014 a las 16:03:25
-- Versión del servidor: 5.5.38-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `webTopWear`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Admin`
--

CREATE TABLE IF NOT EXISTS `Admin` (
`id` int(11) NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fechaIngreso` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `Admin`
--

INSERT INTO `Admin` (`id`, `usuario`, `password`, `fechaIngreso`) VALUES
(1, 'admin', 'admin', '0000-00-00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Campania`
--

CREATE TABLE IF NOT EXISTS `Campania` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaIngreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Categorias`
--

CREATE TABLE IF NOT EXISTS `Categorias` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descripcion` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `Categorias`
--

INSERT INTO `Categorias` (`id`, `nombre`, `descripcion`, `fecha`) VALUES
(2, 'Categoriauno', 'Descripcionuno', '2014-08-29'),
(3, 'categoriados', 'categoriados', '2014-08-29'),
(4, 'categoriatres', 'categoriatres', '2014-08-29'),
(5, 'categoriacuatro', 'categoriacuatro', '2014-08-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FotoCampania`
--

CREATE TABLE IF NOT EXISTS `FotoCampania` (
`id` int(11) NOT NULL,
  `campania_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaIngreso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `FotoProducto`
--

CREATE TABLE IF NOT EXISTS `FotoProducto` (
`id` int(11) NOT NULL,
  `productos_id` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaIngreso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productos`
--

CREATE TABLE IF NOT EXISTS `Productos` (
`id` int(11) NOT NULL,
  `subcategorias_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descripcionCorta` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicauno` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicados` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicatres` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicacuatro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicacinco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteriscaseis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechaIngreso` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `SubCategorias`
--

CREATE TABLE IF NOT EXISTS `SubCategorias` (
`id` int(11) NOT NULL,
  `categorias_id` int(11) DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `SubCategorias`
--

INSERT INTO `SubCategorias` (`id`, `categorias_id`, `nombre`, `fecha`) VALUES
(2, 2, 'subcategoriatres', '2014-08-29'),
(3, 3, 'subcategoriados', '2014-08-29'),
(4, 3, 'categoriadossubcategoriauno', '2014-08-29'),
(5, 3, 'categoriadossubcategoriados', '2014-08-29'),
(6, 3, 'categoriadossubcategoriatres', '2014-08-29'),
(7, 4, 'categoriatressubcategoriauno', '2014-08-29'),
(8, 4, 'categoriatressubcategoriados', '2014-08-29'),
(9, 4, 'categoriatressubcategoriatres', '2014-08-29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Tiendas`
--

CREATE TABLE IF NOT EXISTS `Tiendas` (
`id` int(11) NOT NULL,
  `sucursal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fechaIngreso` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Admin`
--
ALTER TABLE `Admin`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Campania`
--
ALTER TABLE `Campania`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `Categorias`
--
ALTER TABLE `Categorias`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `FotoCampania`
--
ALTER TABLE `FotoCampania`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_93ED546EC6F69FB` (`campania_id`);

--
-- Indices de la tabla `FotoProducto`
--
ALTER TABLE `FotoProducto`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_2030663CED07566B` (`productos_id`);

--
-- Indices de la tabla `Productos`
--
ALTER TABLE `Productos`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_F48512451A9B97E8` (`subcategorias_id`);

--
-- Indices de la tabla `SubCategorias`
--
ALTER TABLE `SubCategorias`
 ADD PRIMARY KEY (`id`), ADD KEY `IDX_FD2D2CEA5792B277` (`categorias_id`);

--
-- Indices de la tabla `Tiendas`
--
ALTER TABLE `Tiendas`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Admin`
--
ALTER TABLE `Admin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `Campania`
--
ALTER TABLE `Campania`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Categorias`
--
ALTER TABLE `Categorias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `FotoCampania`
--
ALTER TABLE `FotoCampania`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `FotoProducto`
--
ALTER TABLE `FotoProducto`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `Productos`
--
ALTER TABLE `Productos`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `SubCategorias`
--
ALTER TABLE `SubCategorias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `Tiendas`
--
ALTER TABLE `Tiendas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `FotoCampania`
--
ALTER TABLE `FotoCampania`
ADD CONSTRAINT `FK_93ED546EC6F69FB` FOREIGN KEY (`campania_id`) REFERENCES `Campania` (`id`);

--
-- Filtros para la tabla `FotoProducto`
--
ALTER TABLE `FotoProducto`
ADD CONSTRAINT `FK_2030663CED07566B` FOREIGN KEY (`productos_id`) REFERENCES `Productos` (`id`);

--
-- Filtros para la tabla `Productos`
--
ALTER TABLE `Productos`
ADD CONSTRAINT `FK_F48512451A9B97E8` FOREIGN KEY (`subcategorias_id`) REFERENCES `SubCategorias` (`id`);

--
-- Filtros para la tabla `SubCategorias`
--
ALTER TABLE `SubCategorias`
ADD CONSTRAINT `FK_FD2D2CEA5792B277` FOREIGN KEY (`categorias_id`) REFERENCES `Categorias` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
